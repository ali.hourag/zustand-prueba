
import './App.css'
import Button from './components/Button';
import Counter from './components/Counter';
import CounterInfo from './components/CounterInfo';
import Users from './components/Users';

function App() {

  return (
    <>
      <Button />
      <Counter />
      <Users/>
      <CounterInfo />
    </>
  )
}

export default App
