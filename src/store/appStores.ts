import create from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';

export type UserInfoType = {
    name: string;
    email: string;
    password: string;
}

interface AppState {
    users: UserInfoType[],
    addUser: (user: UserInfoType) => void,
    userInfo: UserInfoType,
    setUserInfo: (userInfo: UserInfoType) => void;
    userLogged: boolean;
    setUserLogged: () => void;
    count: number;
    setCount: () => void;
}

export const useAppStore = create<AppState>()(persist((set) => {
    return {
        users: [],
        addUser: (user: UserInfoType) => set((state) => ({ users: [...state.users, user] })),
        userInfo: {
            name: 'example',
            email: 'example@example.com',
            password: '1234',
        },
        setUserInfo: (userInfo: UserInfoType) => set(() => ({ userInfo })),
        userLogged: false,
        setUserLogged: () => set((state) => ({ userLogged: !state.userLogged })),
        count: 0,
        setCount: () => set((state) => ({ count: state.count + 1 })),
    }
}, {
    name: 'app-storage',
    storage: createJSONStorage(() => sessionStorage),
}))

export default useAppStore;
