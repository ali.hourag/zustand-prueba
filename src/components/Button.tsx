import { useEffect } from "react";
import useAppStore from "../store/appStores";


const Button = () => {
    // With this first approach, it does only render this setUserLogged state
    const userLogged = useAppStore(state => state.userLogged)
    const setUserLogged = useAppStore(state => state.setUserLogged)
    // De esta manera solamente se renderiza el componente cuando cambian estos
    // dos estados en específico, no se renderiza cuando cambian otros estados


    // With this second approach, it does render all the states
    // const { userLogged, setUserLogged } = useAppStore();
    // De esta manera, el componente se renderizará siempre
    // que cambie cualquier estado

    // With this third approach, it does render all the states as well
    // const { userLogged, setUserLogged } = useAppStore(state => state);

    const handleBtnUserChanged = () => {
        setUserLogged();
    }

    useEffect(() => {
        console.log(userLogged);
        console.log("user logged has been changed");
    }, [userLogged])

    // console.log("im in");

    return (
        <div>
            <h3>{userLogged.toString()}</h3>
            <button className='user-btn' onClick={handleBtnUserChanged}>
                Change user logged
            </button>
        </div>
    )
}

export default Button