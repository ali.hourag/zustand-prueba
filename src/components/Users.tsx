
import { useForm } from 'react-hook-form';
// import useAppStore, { UserInfoType } from '../store/appStores';
import styles from './Users.module.css';
import { useEffect } from 'react';

const Users = () => {

    // const users = useAppStore(state => state.users);
    // const addUser = useAppStore(state => state.addUser);

    const { register, handleSubmit, formState: { errors, isValid, dirtyFields, isDirty, touchedFields }, watch, reset } = useForm({
        defaultValues: {
            name: '',
            email: '',
            password: '',
        },
        mode: 'onChange'
    });

    const validateUser = () => {
        const name = watch('name');
        const email = watch('email');
        const password = watch('password');
        console.log(name, email, password);
    };

    useEffect(()=> {
        console.log({
            dirtyFields,
            isDirty,
            touchedFields
        });
    }, [dirtyFields, isDirty, touchedFields])

    return (
        <>
            <form
                onSubmit={handleSubmit(validateUser)}
                className={styles.container}
            >
                <div className={styles.inputContainer}>
                    <input type="text"
                        id='name'
                        className={styles.input}
                        autoComplete='name'
                        placeholder='put your name'
                        {...register('name', {
                            required: {
                                value: true,
                                message: 'name is required'
                            }
                        })}
                    />
                    {errors.name ? <p className={styles.error}>{errors.name.message}</p> : <p className={styles.fakeError}>error</p>}
                </div>
                <div className={styles.inputContainer}>
                    <input type="email"
                        id='email'
                        className={styles.input}
                        placeholder='put your email'
                        autoComplete='email'
                        {...register('email', {
                            required: {
                                value: true,
                                message: 'email is required'
                            },
                            pattern: {
                                value: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g,
                                message: 'invalid email'
                            }
                        })}
                    />
                    {errors.password ? <p className={styles.error}>{errors.password.message}</p> : <p className={styles.fakeError}>error</p>}
                </div>
                <div className={styles.inputContainer}>
                    <input type="password"
                        id='password'
                        className={styles.input}
                        autoComplete='current-password'
                        placeholder='put your password'
                        {...register('password', {
                            required: {
                                value: true,
                                message: 'password is required'
                            },
                            minLength: {
                                value: 8,
                                message: 'password must be at least 8 characters'
                            }
                        })}
                    />
                    {errors.password ? <p className={styles.error}>{errors.password.message}</p> : <p className='styles.fakeError'>error</p>}
                </div>
                <div className={styles.btnContainer}>
                    <button
                        type='submit'
                        disabled={!isValid}
                        className={styles.btn}
                    >Submit
                    </button>

                    <button
                        type='submit'
                        className={styles.btn}
                        onClick={() => reset()}
                    >Reset</button>
                </div>

            </form>
        </>
    );
};

export default Users;
