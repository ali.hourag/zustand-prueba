import { useEffect, useState } from 'react';
import useAppStore, { UserInfoType } from '../store/appStores';
import styles from './Users.module.css';

const CounterInfo: React.FC = () => {

    const setUserInfo = useAppStore(state => state.setUserInfo);
    const userInfo = useAppStore(state => state.userInfo);


    // When this method is used, it is rendered when other state of useAppStore is changed
    // NOT EVERYTHING IS CHANGED, ONLY THE ONES USING THIS METHOD

    // const { setUserInfo, userInfo } = useAppStore();

    const [user, setUser] = useState<UserInfoType>({
        name: '',
        email: '',
        password: '',
    });

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setUser({ ...user, [name]: value });
    };

    const handleSumbitBtnClicked = () => {
        type NewUserInfoType = {
            name?: string;
            email?: string;
            password?: string;
        }
        let newUserInfo: NewUserInfoType = {};
        if (user.name) newUserInfo.name = user.name;
        if (user.email) newUserInfo.email = user.email;
        if (user.password) newUserInfo.password = user.password;

        console.log("i am setting this info", newUserInfo);

        setUserInfo(user);
    }

    useEffect(() => {
        console.log(userInfo);
    }, [userInfo])

    return (
        <>
            <form className={styles.container}>
                <input type="text" name="name" value={user.name} onChange={handleInputChange} autoComplete='username' />
                <input type="email" name="email" value={user.email} onChange={handleInputChange} autoComplete='email' />
                <input type="password" name="password" value={user.password} onChange={handleInputChange} autoComplete='current-password' />
            </form>
            <button onClick={handleSumbitBtnClicked}>Submit</button>
        </>
    );
};

export default CounterInfo;
