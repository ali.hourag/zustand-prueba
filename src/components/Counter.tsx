import useAppStore from '../store/appStores';
import reactLogo from '../assets/react.svg'
import viteLogo from '/vite.svg'
import { useEffect } from 'react';

const Counter = () => {

    // With this first approach, it does only render this count state
    const count = useAppStore(state => state.count)
    const setCount = useAppStore(state => state.setCount)


    // With this second approach, it does render all the states
    // const { count, setCount } = useAppStore();

    // With this third approach, it does render all the states as well
    // const { count, setCount } = useAppStore(state => state)

    useEffect(() => {
        console.log("count has changed");
    }, [count])


    // console.log("entro");


    return (
        <>
            <div>
                <a href="https://vitejs.dev" target="_blank">
                    <img src={viteLogo} className="logo" alt="Vite logo" />
                </a>
                <a href="https://react.dev" target="_blank">
                    <img src={reactLogo} className="logo react" alt="React logo" />
                </a>
            </div>
            <h1>Vite + React</h1>
            <div className="card">
                <button onClick={() => setCount()}>
                    count is {count}
                </button>
                <p>
                    Edit <code>src/App.tsx</code> and save to test HMR
                </p>
            </div>
            <p className="read-the-docs">
                Click on the Vite and React logos to learn more
            </p>
        </>
    )
}

export default Counter